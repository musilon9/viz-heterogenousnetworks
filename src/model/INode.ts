
export default interface INode {
    archetype: number;
    id: number;
    text: string;
    title: string;
    attributes: any;
}
