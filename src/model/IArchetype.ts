
export default interface IArchetype {
    name: string;
    text: string;
}
