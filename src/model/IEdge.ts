
export default interface IEdge {
    archetype: number;
    id: number;
    text: string;
    from: number;
    to: number;
    attributes: any;
}
