import GraphNode from "@/model/GraphNode";
import GraphEdge from "@/model/GraphEdge";

import * as _ from 'lodash';
import INode from "@/model/INode";
import IEdge from "@/model/IEdge";
import IDegreeOfInterestService from "@/services/algorithms/IDegreeOfInterestService";
import SimpleDegreeOfInterestService from "@/services/algorithms/SimpleDegreeOfInterestService";
import {CONSTANTS} from "@/Constants";

export default class Graph {

    nodes: GraphNode[];
    edges: GraphEdge[];

    doiService: IDegreeOfInterestService;

    constructor(nodes: INode[], edges: IEdge[], focusNodeIds: number[] = []) {
        this.nodes = _.map(nodes, (node) => new GraphNode(node, _.includes(focusNodeIds, node.id)));
        this.edges = _.map(edges, (edge) => new GraphEdge(edge));

        this.doiService = new SimpleDegreeOfInterestService();

        this.connectEdges();
        this.initDoiValues();
    }

    public resetDistances() {
        _.each(this.nodes, (node) => {
            node.focusDistance = CONSTANTS.DISTANCE_INFINITY;
        })
    }

    public getTopKNodesByDoi(k: number): GraphNode[] {
        return _.take(
            _.orderBy(this.nodes, 'DOI', 'desc'),
            k);
    }

    public expand(iNodes: INode[], iEdges: IEdge[]) {
        const nodes = iNodes.map((node) => new GraphNode(node));
        const edges = iEdges.map((edge) => new GraphEdge(edge));
        _.each(nodes, (node) => {
            node.isHidden = true;
            node.randomizePosition();
        });
        this.nodes.push(...nodes);
        _.each(edges, (edge) => {
            const nodeFrom = _.find(this.nodes, (node) => node.id === edge.from);
            const nodeTo = _.find(this.nodes, (node) => node.id === edge.to);
            if (nodeFrom && nodeTo) {
                edge.fromNode = nodeFrom;
                edge.toNode = nodeTo;
                nodeFrom.edgesOut.push(edge);
                nodeTo.edgesIn.push(edge);
            }
        });
        this.edges.push(...edges);
    }

    public setExpandableAttributes(expandableNodeIds: number[]) {
        _.each(this.nodes, (node) => {
            node.isExpandable = _.includes(expandableNodeIds, node.id);
        });
    }

    public makeNodesVisible() {
        _.each(this.nodes, (node) => {
            node.isHidden = false;
        });
    }

    public animateToTargetPositions() {
        _.each(this.nodes, (node) => node.animateToTarget());
    }

    private connectEdges() {
        _.each(this.edges, (edge) => {
            const nodeFrom = _.find(this.nodes, (node) => node.id === edge.from);
            const nodeTo = _.find(this.nodes, (node) => node.id === edge.to);
            if (nodeFrom && nodeTo) {
                edge.fromNode = nodeFrom;
                edge.toNode = nodeTo;
                nodeFrom.edgesOut.push(edge);
                nodeTo.edgesIn.push(edge);
            }
        });
    }

    private initDoiValues() {
        _.each(this.nodes, (node) => {
            node.DOI = this.doiService.doi(node);
        });
    }
}
