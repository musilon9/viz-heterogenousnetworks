import IEdge from "@/model/IEdge";
import GraphNode from "@/model/GraphNode";
import IEdgeCoordinates from "@/model/IEdgeCoordinates";

export default class GraphEdge implements IEdge {
    archetype!: number;
    from!: number;
    id!: number;
    text!: string;
    to!: number;
    attributes: any;

    fromNode?: GraphNode;
    toNode?: GraphNode;

    originalEdges: GraphEdge[] = [];

    constructor(edgePartial: Partial<GraphEdge>) {
        Object.assign(this, edgePartial);
    }

    get coordinates(): IEdgeCoordinates {
        if (!this.fromNode || !this.toNode) {
            return {startX: 0, startY: 0, targetX: 0, targetY: 0};
        }

        const ax = this.fromNode.centerX;
        const ay = this.fromNode.centerY;
        const ar = this.fromNode.radius;
        const bx = this.toNode.centerX;
        const by = this.toNode.centerY;
        const br = this.toNode.radius;

        const dx = bx - ax;
        const dy = by - ay;
        const dNorm = Math.sqrt(dx * dx + dy * dy);

        return {
            startX: ax + ar * dx / dNorm,
            startY: ay + ar * dy / dNorm,
            targetX: bx - br * dx / dNorm,
            targetY: by - br * dy / dNorm,
        };
    }

    get isHidden(): boolean {
        if (!this.fromNode || !this.toNode) {
            return true;
        }
        return this.fromNode.isHidden || this.toNode.isHidden;
    }

}
