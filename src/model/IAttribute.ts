
export default interface IAttribute {
    dataType: string;
    name: string;
    text: string;
}
