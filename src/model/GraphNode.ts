import { TweenLite } from "gsap";
import INode from "@/model/INode";
import GraphEdge from "@/model/GraphEdge";
import {CONSTANTS} from "@/Constants";

export default class GraphNode implements INode {

    private static getGraphCenter(): number {
        return (CONSTANTS.SVG_VIEW_BOX_WIDTH / 2) - 2 * CONSTANTS.NODE_RADIUS_DEFAULT;
    }

    archetype!: number;
    id!: number;
    text!: string;
    title!: string;

    attributes: any;
    nameAttributeIndex: number = 1;

    edgesIn: GraphEdge[] = [];
    edgesOut: GraphEdge[] = [];
    focusDistance: number = CONSTANTS.DISTANCE_INFINITY;

    DOI: number = 0;
    isFocusNode: boolean = false;
    isExpandable: boolean = false;
    isHidden: boolean = false;

    targetX: number = GraphNode.getGraphCenter();
    targetY: number = GraphNode.getGraphCenter();
    dispX: number = 0;
    dispY: number = 0;
    centerX: number = this.targetX;
    centerY: number = this.targetY;

    constructor(nodePartial: Partial<GraphNode>, isFocusNode: boolean = false) {
        Object.assign(this, nodePartial);
        this.isFocusNode = isFocusNode;
    }

    get degree(): number {
        return this.edgesIn.length + this.edgesOut.length;
    }

    get name(): string {
        return this.attributes[this.nameAttributeIndex] || this.title;
    }

    /**
     * Node radius could depend on importance of the node
     */
    get radius(): number {
        return CONSTANTS.NODE_RADIUS_DEFAULT;
    }

    public randomizePosition() {
        const viewBoxSize = CONSTANTS.SVG_VIEW_BOX_WIDTH - 4 * CONSTANTS.NODE_RADIUS_DEFAULT;
        this.targetX = Math.random() * viewBoxSize;
        this.targetY = Math.random() * viewBoxSize;
        this.centerX = this.targetX;
        this.centerY = this.targetY;
    }

    public animateToTarget() {
        console.log("animating", this);
        TweenLite.to(this, CONSTANTS.TRANSITION_INTERVAL_MILLIS / 1000, {centerX: this.targetX, centerY: this.targetY});
    }

}
