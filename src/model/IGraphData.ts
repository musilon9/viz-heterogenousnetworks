import INode from '@/model/INode';
import IEdge from '@/model/IEdge';

export default interface IGraphData {
    nodes: INode[];
    edges: IEdge[];
    focusNodeIds: number[];
    expandableNodeIds: number[];
}
