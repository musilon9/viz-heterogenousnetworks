
export default interface IEdgeCoordinates {
    startX: number;
    startY: number;
    targetX: number;
    targetY: number;
}
