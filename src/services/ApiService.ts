import INode from '@/model/INode';
import * as _ from 'lodash';
import {CONSTANTS} from '@/Constants';
import IArchetype from '@/model/IArchetype';
import IAttribute from '@/model/IAttribute';
import IEdge from "@/model/IEdge";
import IGraphData from '@/model/IGraphData';
import Graph from '@/model/Graph';

export default abstract class ApiService {

    public static validateTextQuery(value: string): boolean {
        const words = _.words(value);
        return _.sumBy(words, (word) => word.length) >= CONSTANTS.SEARCH_QUERY_MIN_LENGTH;
    }

    public abstract getNodeArchetypes(): Promise<IArchetype[]>;

    public abstract getEdgeArchetypes(): Promise<IArchetype[]>;

    public abstract getAttributeTypes(): Promise<IAttribute[]>;

    public abstract findNodesByTextQuery(query: string): Promise<INode[]>;

    public abstract getKMostInterestingNodes(k: number): Promise<INode[]>;

    public abstract getRandomNeighborOf(id: number): Promise<{ neighbor: INode, edge: IEdge }>;

    public abstract getInitialSubgraph(params: any): Promise<IGraphData>;

    public abstract getExpandedSubgraph(currentGraph: Graph, expandedNodeId: number): Promise<IGraphData>;

}

