import GraphNode from "@/model/GraphNode";

export default interface IDegreeOfInterestService {

    /**
     * A-priori interest of node
     * @param node
     */
    api(node: GraphNode): number;

    /**
     * Degree of interest of node
     * @param node
     */
    doi(node: GraphNode): number;
}
