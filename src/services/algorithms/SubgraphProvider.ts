import * as _ from 'lodash';
import * as Collections from 'typescript-collections';
import Graph from '@/model/Graph';
import {CONSTANTS} from '@/Constants';
import GraphNode from '@/model/GraphNode';
import GraphEdge from '@/model/GraphEdge';
import IGraphData from '@/model/IGraphData';
import IDegreeOfInterestService from "@/services/algorithms/IDegreeOfInterestService";
import SimpleDegreeOfInterestService from "@/services/algorithms/SimpleDegreeOfInterestService";

export default class SubgraphProvider {

    graph: Graph;
    doiService: IDegreeOfInterestService = new SimpleDegreeOfInterestService();

    constructor(graph: Graph) {
        this.graph = graph;
    }

    generateInitialSubgraph(kFocusNodes: number, initSize: number): IGraphData {
        const focusNodes = this.graph.getTopKNodesByDoi(kFocusNodes);
        const focusNodeIds = _.map(focusNodes, 'id');

        const q = new Collections.PriorityQueue<GraphNode>((a, b) => a.DOI - b.DOI);

        this.graph.resetDistances();
        _.each(focusNodes, (node) => {
            node.focusDistance = 0;
            node.DOI = this.doiService.doi(node);
            q.add(node);
        });

        const nodes: GraphNode[] = [];
        const edges: GraphEdge[] = [];
        let graphSize = 0;
        while (graphSize < initSize && !q.isEmpty()) {
            const v = q.dequeue()!;
            if (nodes.includes(v)) {
                continue;
            }
            _.each(v.edgesOut, (edge) => {
                const w = edge.toNode!;
                if (w.focusDistance === CONSTANTS.DISTANCE_INFINITY) {
                    w.focusDistance = v.focusDistance + 1;
                    w.DOI = this.doiService.doi(w);
                    q.add(w);
                }
            });
            nodes.push(v);
            graphSize++;
        }

        const relevantEdges = _.filter(
            this.graph.edges,
            (edge) => _.some(nodes, ['id', edge.from]) && _.some(nodes, ['id', edge.to])
        );
        const mainEdges = _.uniqWith(
            relevantEdges,
            (e1, e2) => _.isEmpty(_.difference([e1.from, e1.to], [e2.from, e2.to]))
        );
        edges.push(...mainEdges);
        _.each(edges, (edge) => {
            edge.originalEdges = _.filter(
                relevantEdges,
                (e) => _.isEmpty(_.difference([e.from, e.to], [edge.from, edge.to]))
            );
        });

        const expandableNodeIds = _.map(
            _.filter(
                nodes,
                (node) => !_.isEmpty(
                    _.difference(
                        _.map(node.edgesOut, (edge) => edge.toNode!.id),
                        _.map(nodes, 'id')
                    )
                )
            ),
            'id'
        );

        return {nodes, edges, focusNodeIds, expandableNodeIds};
    }

    generateSubgraphExpansion(currentGraph: Graph, expandedNodeId: number): IGraphData {
        const currentNodeIds = _.map(currentGraph.nodes, 'id');

        const v = _.find(this.graph.nodes, ['id', expandedNodeId])!;

        const nodesDiff: GraphNode[] = _.chain(v.edgesOut)
            .map('toNode')
            .compact()
            .uniqBy('id')
            .filter((node) => !_.includes(currentNodeIds, node.id))
            .map((node) => {
                node.focusDistance = v.focusDistance + 1;
                node.DOI = this.doiService.doi(node);
                return node;
            })
            .orderBy('DOI', 'desc')
            .take(CONSTANTS.EXPANSION_SIZE)
            .value();

        const edgesDiff: GraphEdge[] = [];
        _.each(nodesDiff, (node) => {
                const edgesBetween = _.concat(
                    _.filter(v.edgesOut, ['to', node.id]),
                    _.filter(v.edgesIn, ['from', node.id])
                );
                const mainEdge = edgesBetween[0];
                mainEdge.originalEdges = edgesBetween;
                edgesDiff.push(mainEdge);
        });

        const allNodes = _.concat(currentGraph.nodes, nodesDiff);
        const expandableNodeIds = _.map(
            _.filter(
                allNodes,
                (node) => !_.isEmpty(
                    _.difference(
                        _.map(node.edgesOut, (edge) => edge.toNode!.id),
                        _.map(allNodes, 'id')
                    )
                )
            ),
            'id'
        );
        console.log("Adding", nodesDiff.length, "nodes");
        return {nodes: nodesDiff, edges: edgesDiff, focusNodeIds: [], expandableNodeIds};
    }
}
