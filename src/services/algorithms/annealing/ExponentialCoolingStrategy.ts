import CoolingStrategy from '@/services/algorithms/annealing/CoolingStrategy';

export default class ExponentialCoolingStrategy implements CoolingStrategy {

    maxLimit: number;
    iterations: number;

    constructor(maxLimit: number, iterations: number = 5) {
        this.maxLimit = maxLimit;
        this.iterations = iterations;
    }

    getLimits(): number[] {
        const limits = [];
        let limit = this.maxLimit;
        for (let i = 0; i < this.iterations; i++) {
            limits.push(limit);
            limit /= 2;
        }
        return limits;
    }

}
