import CoolingStrategy from '@/services/algorithms/annealing/CoolingStrategy';
import * as _ from 'lodash';

export default class QSCoolingStrategy implements CoolingStrategy {

    constructor(private startingTemperature: number,
                private targetTemperature: number,
                private quenchingIterations: number,
                private simmeringIterations: number) {
    }

    getLimits(): number[] {
        return _.concat(
            _.range(
                this.startingTemperature,
                this.targetTemperature,
                (this.targetTemperature - this.startingTemperature) / this.quenchingIterations),
            _.times(this.simmeringIterations, _.constant(this.targetTemperature))
        );
    }

}

