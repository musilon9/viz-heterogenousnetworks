
export default interface CoolingStrategy {
    getLimits(): number[];
}
