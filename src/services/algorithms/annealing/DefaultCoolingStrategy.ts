import CoolingStrategy from '@/services/algorithms/annealing/CoolingStrategy';

export default class DefaultCoolingStrategy implements CoolingStrategy {

    getLimits(): number[] {
        return [];
    }

}

