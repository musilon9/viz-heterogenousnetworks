import IDegreeOfInterestService from "@/services/algorithms/IDegreeOfInterestService";
import GraphNode from "@/model/GraphNode";
import {CONSTANTS} from '@/Constants';

export default class SimpleDegreeOfInterestService implements IDegreeOfInterestService {
    api(node: GraphNode): number {
        return Math.log2(Math.min(CONSTANTS.DEGREE_INFINITY, node.degree));
    }

    dist(node: GraphNode): number {
        return -Math.log2(Math.min(CONSTANTS.DISTANCE_INFINITY, 1 + node.focusDistance));
    }

    doi(node: GraphNode): number {
        return CONSTANTS.API_COEFFICIENT * this.api(node) + CONSTANTS.DIST_COEFFICIENT * this.dist(node);
    }

}
