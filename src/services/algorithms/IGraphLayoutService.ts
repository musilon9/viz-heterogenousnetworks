
export default interface IGraphLayoutService {
    layoutGraph(): void;
}
