import Graph from '@/model/Graph';
import {CONSTANTS} from '@/Constants';
import * as _ from 'lodash';
import IGraphLayoutService from '@/services/algorithms/IGraphLayoutService';
import CoolingStrategy from '@/services/algorithms/annealing/CoolingStrategy';

export default class ForceBasedLayoutService implements IGraphLayoutService {

    graph: Graph;
    areaWidth: number = CONSTANTS.SVG_VIEW_BOX_WIDTH - 4 * CONSTANTS.NODE_RADIUS_DEFAULT;
    areaHeight: number = CONSTANTS.SVG_VIEW_BOX_HEIGHT - 4 * CONSTANTS.NODE_RADIUS_DEFAULT;
    kay: number;
    coolingStrategy: CoolingStrategy;
    limits: number[] = [];

    constructor(graph: Graph, coolingStrategy: CoolingStrategy, kayMultiplier: number = 1) {
        this.graph = graph;
        this.coolingStrategy = coolingStrategy;
        // Optimal distance between two nodes k := C * sqrt( area / |V| )
        this.kay = kayMultiplier * Math.sqrt(this.areaWidth * this.areaHeight / this.graph.nodes.length);
        this.limits = this.coolingStrategy.getLimits();
    }

    /**
     * Optimal distance between two nodes
     * k := C * sqrt( area / |V| )
     */

    attr_force(distance: number): number {
        return distance ** 2 / this.kay;
    }

    rep_force(distance: number): number {
        return this.kay ** 2 / distance;
    }

    layoutGraph() {
        _.each(this.limits, (limit) => {
            _.each(this.graph.nodes, (v) => {
                v.dispX = 0;
                v.dispY = 0;
                _.each(this.graph.nodes, (w) => {
                    if (w.id === v.id) {
                        return;
                    }
                    const deltaX = v.targetX - w.targetX;
                    const deltaY = v.targetY - w.targetY;
                    const dist = Math.sqrt(deltaX ** 2 + deltaY ** 2);
                    const rep = this.rep_force(dist);
                    v.dispX += deltaX / dist * rep;
                    v.dispY += deltaY / dist * rep;
                });
            });

            _.each(this.graph.edges, (e) => {
                const v = e.fromNode!;
                const w = e.toNode!;
                const deltaX = v.targetX - w.targetX;
                const deltaY = v.targetY - w.targetY;
                const dist = Math.sqrt(deltaX ** 2 + deltaY ** 2);
                const attr = this.attr_force(dist);
                v.dispX -= deltaX / dist * attr;
                v.dispY -= deltaY / dist * attr;
                w.dispX += deltaX / dist * attr;
                w.dispY += deltaY / dist * attr;
            });

            _.each(this.graph.nodes, (v) => {
                const dispNorm = Math.sqrt(v.dispX ** 2 + v.dispY ** 2);
                v.targetX += v.dispX / dispNorm * Math.min(dispNorm, limit);
                v.targetY += v.dispY / dispNorm * Math.min(dispNorm, limit);
                v.targetX = Math.min(this.areaWidth, Math.max(0, v.targetX));
                v.targetY = Math.min(this.areaHeight, Math.max(0, v.targetY));
            });

        });
    }
}

