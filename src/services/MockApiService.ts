import * as graphData from '../assets/graphs/SW-eng-anonymized-demo-graph.json';
import * as _ from 'lodash';
import {remove as removeDiacritics} from 'diacritics';

import IAttribute from '@/model/IAttribute';
import INode from '@/model/INode';
import IArchetype from '@/model/IArchetype';
import ApiService from '@/services/ApiService';
import Graph from "@/model/Graph";
import IEdge from "@/model/IEdge";
import SubgraphProvider from '@/services/algorithms/SubgraphProvider';
import IGraphData from '@/model/IGraphData';


export default class MockApiService extends ApiService {

    public static getInstance(): MockApiService {
        if (!this.instance) {
            this.instance = new MockApiService();
        }
        return this.instance;
    }
    private static instance?: MockApiService;

    private graph: Graph;
    private subgraphProvider: SubgraphProvider;

    private constructor() {
        super();
        this.graph = new Graph(graphData.vertices, graphData.edges);
        this.subgraphProvider = new SubgraphProvider(this.graph);
    }

    public getNodeArchetypes(): Promise<IArchetype[]> {
        return Promise.resolve(graphData.vertexArchetypes);
    }

    public getEdgeArchetypes(): Promise<IArchetype[]> {
        return Promise.resolve(graphData.edgeArchetypes);
    }

    public getAttributeTypes(): Promise<IAttribute[]> {
        return Promise.resolve(graphData.attributeTypes);
    }

    public findNodesByTextQuery(query: string): Promise<INode[]> {
        const words = _.words(query);
        if (!ApiService.validateTextQuery(query)) {
            return Promise.reject('Search query is not valid.');
        }

        const normalized = (str: string) => _.toLower(removeDiacritics(str));
        const nodes = _.filter(
            graphData.vertices,
            (node) => _.every(
                words,
                (word) => normalized(JSON.stringify(node)).includes((normalized(word)))));

        return Promise.resolve(nodes);
    }

    public getKMostInterestingNodes(k: number): Promise<INode[]> {
        return Promise.resolve(this.graph.getTopKNodesByDoi(k));
    }

    public getRandomNeighborOf(id: number): Promise<{ neighbor: INode; edge: IEdge }> {
        const currentNode = _.find(graphData.vertices, (v) => v.id === id);
        if (currentNode) {
            const edge = _.find(graphData.edges, (e) => e.from === currentNode.id);
            if (edge) {
                const neighbor = _.find(graphData.vertices, (v) => v.id === edge.to);
                if (neighbor) {
                    return Promise.resolve({neighbor, edge});
                }
            }
        }
        return Promise.reject('Neighbor not found');
    }

    public getInitialSubgraph(params: any): Promise<IGraphData> {
        const {focusNodes, initialSize} = params;
        return Promise.resolve(this.subgraphProvider.generateInitialSubgraph(focusNodes, initialSize));
    }

    public getExpandedSubgraph(currentGraph: Graph, expandedNodeId: number): Promise<IGraphData> {
        return Promise.resolve(this.subgraphProvider.generateSubgraphExpansion(currentGraph, expandedNodeId));
    }
}
