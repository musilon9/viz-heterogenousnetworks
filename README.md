# networks

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Useful links (Technologies)

- [Vue.js](https://vuejs.org)
- [TypeScript](https://typescriptlang.org)
- [Bootstrap-Vue](https://bootstrap-vue.js.org)
- [Lodash](https://lodash.com)
- [Typescript Collections](https://www.npmjs.com/package/typescript-collections)

### TODO
- Implement force-based layout
- Provide options for editing constants in GUI
- DONE Display node/edge data on after single click on that node/edge
- Graphical improvements (colors, highlights etc.)
